package com.parkinglot;

public class ParkingBoy {

    private ParkingLot parkingLot;

    public ParkingBoy() {
    }

    public ParkingTicket helpParking(Car car) {
        ParkingLot parkingLot = new ParkingLot();
        return parkingLot.park(car);
    }

    public Car helpFetch(ParkingTicket parkingTicket) {
        ParkingLot parkingLot = new ParkingLot();
        return parkingLot.fetch(parkingTicket);
    }
}
