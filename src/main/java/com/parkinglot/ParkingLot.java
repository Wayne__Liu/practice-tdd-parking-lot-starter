package com.parkinglot;

import java.util.HashMap;

public class ParkingLot {

    private ParkingTicket parkingTicket;
    private int carPosition = 10;
    private Car car;
    private HashMap<ParkingTicket, Car> map;

    public ParkingLot() {
        this.map = new HashMap<ParkingTicket, Car>();
    }

    public ParkingTicket park(Car car) {
        if (carPosition < 1) {
            throw new UnexpectedProjectTypeException("No available position");
        }
        ParkingTicket parkingTicket = new ParkingTicket();
        map.put(parkingTicket, car);
        carPosition--;

        return parkingTicket;
    }

    public Car fetch(ParkingTicket parkingTicket) {
        if (parkingTicket == null) {
            throw new UnexpectedProjectTypeException("Unrecognized parking ticket");
        } else if (map.containsKey(parkingTicket)) {
            Car car1 = map.get(parkingTicket);
            carPosition++;
            map.remove(parkingTicket);
            return car1;
        } else
            throw new UnexpectedProjectTypeException("Unrecognized parking ticket");
    }


}
