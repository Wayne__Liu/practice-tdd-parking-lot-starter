package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ParkingBoyTest {
    @Test
    public void should_return_a_parking_ticket_when_park_given_a_car_and_a_parking_lot_and_a_standard_parking_boy(){
        Car car = new Car();
        ParkingTicket parkingTicket = new ParkingBoy().helpParking(car);
        Assertions.assertNotNull(parkingTicket);
    }

    @Test
    public void should_return_a_parked_car_when_fetch_given_a_car_and_a_parking_lot_and_a_standard_parking_boy_and_a_parked_car(){
        Car car1 = new Car();
        ParkingTicket parkingTicket = new ParkingBoy().helpParking(car1);
        Car car2 =new ParkingBoy().helpFetch(parkingTicket);

        Assertions.assertEquals(car1,car2);
    }

    @Test
    public void should_return_a_parked_car_when_fetch_the_car_twice_given_a_car_and_a_parking_lot_and_a_standard_parking_boy_and_two_parked_car(){
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingTicket parkingTicket1 = parkingLot.park(car1);
        ParkingTicket parkingTicket2 = parkingLot.park(car2);

        Car fetch1 = parkingLot.fetch(parkingTicket1);
        Car fetch2 = parkingLot.fetch(parkingTicket2);

        Assertions.assertEquals(car1, fetch1);
        Assertions.assertEquals(car2, fetch2);
    }

    @Test
    public void should_return_error_message_when_fetch_given_a_parking_lot_and_a_standard_parking_boy_and_a_wrong_parking_ticket(){
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingTicket parkingTicket1 = new ParkingTicket();

//        Assertions.assertEquals(car1,"null");
        UnexpectedProjectTypeException unexpectedProjectTypeException = assertThrows(UnexpectedProjectTypeException.class, () -> parkingLot.fetch(parkingTicket1));

        Assertions.assertEquals("Unrecognized parking ticket", unexpectedProjectTypeException.getMessage());
    }
    @Test
    public void should_return_error_message_when_fetch_given_a_parking_lot_and_a_standard_parking_boy_and_a_used_parking_ticket(){
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingTicket parkingTicket = parkingLot.park(car);
        Car car1 = parkingLot.fetch(parkingTicket);

        UnexpectedProjectTypeException unexpectedProjectTypeException = assertThrows(UnexpectedProjectTypeException.class, () -> parkingLot.fetch(parkingTicket));

        Assertions.assertEquals("Unrecognized parking ticket", unexpectedProjectTypeException.getMessage());

    }
    @Test
    public void should_return_error_message_when_park_given_a_parking_lot_and_a_standard_parking_boy_and_a_car_and_without_any_position(){
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot();

        ParkingTicket parkingTicket1 = parkingLot.park(car1);
        ParkingTicket parkingTicket2 = parkingLot.park(car2);
        UnexpectedProjectTypeException unexpectedProjectTypeException = assertThrows(UnexpectedProjectTypeException.class, () -> parkingLot.fetch(parkingTicket2));

        Assertions.assertEquals("No available position", unexpectedProjectTypeException.getMessage());
    }


    @Test
    public void should_return_a_parking_ticket_when_park_given_a_car_and_two_parking_lot_and_a_standard_parking_boy(){
        Car car = new Car();
        ParkingTicket parkingTicket = new ParkingBoy().helpParking(car);
        Assertions.assertNotNull(parkingTicket);
    }

    @Test
    public void should_return_a_parked_car_when_fetch_given_a_car_and_two_parking_lot_and_a_standard_parking_boy_and_a_parked_car(){
        Car car1 = new Car();
        ParkingTicket parkingTicket = new ParkingBoy().helpParking(car1);
        Car car2 =new ParkingBoy().helpFetch(parkingTicket);

        Assertions.assertEquals(car1,car2);
    }

    @Test
    public void should_return_a_parked_car_with_each_ticket_when_fetch_the_car_twice_given_a_car_and_two_parking_lot_and_a_standard_parking_boy_and_two_parked_car(){
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingTicket parkingTicket1 = parkingLot.park(car1);
        ParkingTicket parkingTicket2 = parkingLot.park(car2);

        Car fetch1 = parkingLot.fetch(parkingTicket1);
        Car fetch2 = parkingLot.fetch(parkingTicket2);

        Assertions.assertEquals(car1, fetch1);
        Assertions.assertEquals(car2, fetch2);
    }

    @Test
    public void should_return_error_message_when_fetch_given_two_parking_lot_and_a_standard_parking_boy_and_a_wrong_parking_ticket(){
        ParkingLot parkingLot = new ParkingLot();
        ParkingTicket parkingTicket1 = new ParkingTicket();

//        Assertions.assertEquals(car1,"null");
        UnexpectedProjectTypeException unexpectedProjectTypeException = assertThrows(UnexpectedProjectTypeException.class, () -> parkingLot.fetch(parkingTicket1));

        Assertions.assertEquals("Unrecognized parking ticket", unexpectedProjectTypeException.getMessage());
    }
    @Test
    public void should_return_error_message_when_fetch_given_two_parking_lot_and_a_standard_parking_boy_and_a_used_parking_ticket(){
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingTicket parkingTicket = parkingLot.park(car);
        Car car1 = parkingLot.fetch(parkingTicket);

        UnexpectedProjectTypeException unexpectedProjectTypeException = assertThrows(UnexpectedProjectTypeException.class, () -> parkingLot.fetch(parkingTicket));

        Assertions.assertEquals("Unrecognized parking ticket", unexpectedProjectTypeException.getMessage());

    }
    @Test
    public void should_return_error_message_when_park_given_two_parking_lot_and_a_standard_parking_boy_and_a_car_and_without_any_position(){
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot();

        ParkingTicket parkingTicket1 = parkingLot.park(car1);
        ParkingTicket parkingTicket2 = parkingLot.park(car2);

//        Assertions.assertEquals(car1,car);
//        Assertions.assertEquals(car2,null);
        UnexpectedProjectTypeException unexpectedProjectTypeException = assertThrows(UnexpectedProjectTypeException.class, () -> parkingLot.fetch(parkingTicket2));

        Assertions.assertEquals("No available position", unexpectedProjectTypeException.getMessage());
    }

}
