package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ParkingLotTest {

    @Test
    public void should_return_a_parking_ticket_when_park_given_a_car_and_a_parking_lot(){
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingTicket parkingTicket = parkingLot.park(car);

        Assertions.assertNotNull(parkingTicket);
    }

    @Test
    public void should_return_the_car_when_park_given_a_parking_ticket_and_a_parked_car_and_a_parking_lot(){
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingTicket parkingTicket = parkingLot.park(car);
        Car car1 = parkingLot.fetch(parkingTicket);


        Assertions.assertNotNull(parkingTicket);
        Assertions.assertEquals(car1,car);
    }


    @Test
    public void should_return_the_right_car_when_park_given_a_parking_ticket_and_a_parked_car_and_a_parking_lot(){
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingTicket parkingTicket = parkingLot.park(car);
        Car car1 = parkingLot.fetch(parkingTicket);


        Assertions.assertNotNull(parkingTicket);
        Assertions.assertEquals(car1,car);
    }

    @Test
    public void should_return_error_message_when_fetch_the_car_given_a_wrong_parking_ticket_and_a_parking_lot(){
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingTicket parkingTicket1 = new ParkingTicket();

//        Assertions.assertEquals(car1,"null");
        UnexpectedProjectTypeException unexpectedProjectTypeException = assertThrows(UnexpectedProjectTypeException.class, () -> parkingLot.fetch(parkingTicket1));

        Assertions.assertEquals("Unrecognized parking ticket", unexpectedProjectTypeException.getMessage());
    }

    @Test
    public void should_return_error_message_when_fetch_the_car_given_a_used_parking_ticket_and_a_parking_lot(){
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingTicket parkingTicket = parkingLot.park(car);
        Car car1 = parkingLot.fetch(parkingTicket);

        UnexpectedProjectTypeException unexpectedProjectTypeException = assertThrows(UnexpectedProjectTypeException.class, () -> parkingLot.fetch(parkingTicket));

        Assertions.assertEquals("Unrecognized parking ticket", unexpectedProjectTypeException.getMessage());

    }

    @Test
    public void should_return_null_when_park_the_car_given_a_parking_lot_without_any_position(){
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot();

        ParkingTicket parkingTicket1 = parkingLot.park(car1);
        ParkingTicket parkingTicket2 = parkingLot.park(car2);

//        Assertions.assertEquals(car1,car);
//        Assertions.assertEquals(car2,null);
        UnexpectedProjectTypeException unexpectedProjectTypeException = assertThrows(UnexpectedProjectTypeException.class, () -> parkingLot.fetch(parkingTicket2));

        Assertions.assertEquals("No available position", unexpectedProjectTypeException.getMessage());

    }
}
